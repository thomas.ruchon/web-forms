export const createReclamationECommerce = (
  civilite,
  lastname,
  firstname,
  email,
  phoneNumber,
  mobileNumber,
  orderNumber,
  description
) => {

  console.log(description);
  
  fetch("https://norautoint1602687113.zendesk.com/api/v2/tickets.json", {
    method: "POST",
    headers: {
      "content-type": "application/json",
      Authorization:
        "Bearer eb25b3724fd623b35fa1550b00b3c7e483e7c326e0f7b7a6bbb560031af54a01",
    },
    body: JSON.stringify({
      ticket: {
        subject: `Réclamation commande ${orderNumber}`,
        comment: {
          body: description,
        },
        ticket_form_id: 360001155359,
        custom_fields: [
          { id: 360015614360, value: civilite },
          { id: 360015614040, value: lastname },
          { id: 360015613880, value: firstname },
          { id: 360015614380, value: email },
          { id: 360015576719, value: phoneNumber },
          { id: 360015614880, value: mobileNumber },
          { id: 360015576739, value: orderNumber },
        ],
      },
    }),
  });
};
