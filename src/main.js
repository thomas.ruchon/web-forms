import Vue from 'vue'
import App from './App.vue'

import '@roadtrip/components/dist/roadtrip/roadtrip.css';
import { applyPolyfills, defineCustomElements } from '@roadtrip/components/dist/loader';
import { addIcons } from '@roadtrip/components';
import * as allIcons from '@roadtrip/components/icons';
import _ from 'lodash';
addIcons(_.mapKeys(allIcons, (value, key) => _.kebabCase(key)));

Vue.config.productionTip = false;

Vue.config.ignoredElements = [/road-\w*/];

applyPolyfills().then(() => {
  defineCustomElements(window);
});


new Vue({
  render: h => h(App),
}).$mount('#app')
